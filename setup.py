from setuptools import setup, find_packages
from glob import glob

setup(
    name="octo-uploadd",
    version="1.0",
    packages=find_packages(),
    scripts=['octo-uploadd/octo-uploadd.py'],
    package_data={'octo-uploadd': []},
    data_files=[('/usr/bin', glob('octo-uploadd/octo-uploadd.py'))],
    install_requires=["cherrypy", "pyinotify", "pydbus"]
)
